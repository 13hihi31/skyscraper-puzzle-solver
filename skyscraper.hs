
import Debug.Trace

data Type = ROW | COLUMN deriving Show

{-
  first : type, is it a ROW or COLUMN constraint
  second : index of the row or column in the grid
  third : list of all rows or columns that satisfy this one constraint
-}
type Solutions = (Type, Int, [[Int]])

{- 
  a constraint specifies if it is a row or column constraint, 0 - row constraint, 1 - column constraint
  the index of the row or column in the grid
  the sum of visible skyscrapers, if it is positive we look in the direction of growing indices
  if it is negative we look in the oposite direction
-}
type Constraint = (Int, Int, Int)
type Grid = [[Int]]

get :: Grid -> Int -> Int -> Int
get grid row col = (grid !! row) !! col

size :: Grid -> Int
size grid = length grid

{-
  arg 0 : string to be split
  arg 1 : character seperator
  split the string into list of strings
-}
split :: String -> Char -> [String]
split s c = split1 s c [] []
  where
    split1 :: String -> Char -> String -> [String] -> [String]
    split1 [] c top acc
      | top /= [] = acc ++ [top]
      | otherwise = acc
    split1 (x:xs) c top acc 
      | x == c = split1 xs c [] (if top /= [] then (acc ++ [top]) else acc)
      | otherwise  = split1 xs c (top ++ [x]) acc

triples :: [a] -> [(a, a, a)]
triples (x:y:z:zs) = (x, y, z):triples zs
triples _ = []

{-
  the user provides the sepcifiaction in the following fashion
  size of the grid, constraint 1 (0 for row 1 for col, index of the row or col, number of visible skyscrapers), constraint 2, constraint 3 ...
  example of valid input: "4 0 2 4 0 1 -3 1 0 3 1 2 1"
  yields the following constraints for a 4x4 grid
   -3-1--
   |++++|
   |++++|3
  4|++++|
   |++++|
   ------
-}
trfmStrSpec :: String -> (Int, [(Int, Int, Int)])
trfmStrSpec spec =
  (gridSize, constraints)
  where
    nums = [read x :: Int | x <- split spec ' ']
    gridSize = head nums
    constraints = triples $ tail nums

-- change the string description of the puzzle in to possible solutions list for all constraints specified in the string
decodeGrid :: String -> (Int, [Solutions])
decodeGrid spec = 
  (gridSize, generateSolutions (perms [1..gridSize]) constraints [])
  where
    (gridSize, constraints) = trfmStrSpec spec
  
perms :: Eq a => [a] -> [[a]]
perms [] = [[]]
perms xs = [x:p | x <- xs, p <- perms (remove x xs)]
  where
    remove x [] = []
    remove x (y:ys)
      | x == y = ys
      | otherwise = y : remove x ys

-- for all constraints generate all possible solutions
generateSolutions :: [[Int]] -> [Constraint] -> [Solutions] -> [Solutions]
generateSolutions permutations [] solutions = solutions
generateSolutions permutations (c1:cs1) solutions =
  generateSolutions permutations cs2 $ solutions ++ [filterSolutions permutations c1 c2]
  where
    (s1, x1, h1) = c1
    (c2, cs2) = removeSimilar c1 cs1 []

{-
  arg 0 : permutations of skyscraper heights
  arg 1 : constraint to satify
  arg 2 : second constraint if there is one
  
  from all possible permutations of skyscraper heights in a row or column
  filter the ones that satisfy the constraint
-}
filterSolutions :: [[Int]] -> Constraint -> Maybe Constraint -> Solutions
filterSolutions permutations (s1, x1, h1) Nothing = 
  (if s1 == 0 then ROW else COLUMN, x1, filter (\p -> passConstraint p h1) permutations)
filterSolutions permutations (s1, x1, h1) (Just (_, _, h2)) =
  (if s1 == 0 then ROW else COLUMN, x1, filter (\p -> (passConstraint p h1) && (passConstraint p h2)) permutations)

{-
  arg 0 : the first constraint of the row or column
  arg 1 : constraints not checked yet
  arg 2 : constraints accumulator that are different from the arg 0 constraint
  
  a row and column can have no more than two constraints
  find the second constraint if there is one
-}
removeSimilar :: Constraint -> [Constraint] -> [Constraint] -> (Maybe Constraint, [Constraint])
removeSimilar _ [] different = (Nothing, different)
removeSimilar (s1, x1, h1) ((s2, x2, h2):cs) different
  | x1 == x2 && ((s1 == 0 && s2 == 0) || (s1 == 1  && s2 == 1)) = (Just (s2, x2, h2), different ++ cs)
  | otherwise = removeSimilar (s1, x1, h1) cs ((s2, x2, h2):different)

-- check if the list passes the constraint of the number of visible skyscrapers
passConstraint :: [Int] -> Int -> Bool
passConstraint x h
  | h < 0 = (-h) == visible (reverse x) 0 0
  | otherwise = h == visible x 0 0
  where
    visible :: [Int] -> Int -> Int -> Int
    visible [] v _ = v
    visible (y:ys) v maxh
      | y > maxh = visible ys (v + 1) y
      | otherwise = visible ys v maxh

-- sort list by the key returned by the keyfn for every element in the list
quicksort :: Ord a => (b -> a) -> [b] -> [b]
quicksort _ [] = []
quicksort keyfn (x:xs) = quicksort keyfn smaller ++ x : quicksort keyfn larger
  where
    v = keyfn x
    smaller = [y | y <- xs, keyfn y <= v]
    larger = [y | y <- xs, keyfn y > v]

-- initialy the grid has no skyscrapers which is equivalent to skyscrapers with height 0
initGrid :: Int -> Grid
initGrid gridSize =
  [[0 | x <- [1..gridSize]] | y <- [1..gridSize]]

{-
  solve the puzzle with depth-first search in the solution space
  first search in the solutions of the constraints
  then if the solution was found fill out the gaps left in the grid also with depth-first search
-}
solve1 :: [Solutions] -> Maybe Grid -> Maybe Grid
solve1 [] grid -- all constraints have been eaten up
  | grid == Nothing = Nothing
  | otherwise = fillGaps $ just grid -- by now the constraints are satisfied
solve1 ((t, k, x):s) grid
  | grid == Nothing = Nothing
  | otherwise = solve2 t k x s $ just grid

solve2 :: Type -> Int -> [[Int]] -> [Solutions] -> Grid -> Maybe Grid
solve2 _ _ [] _ _ = Nothing -- we tried all solutions to a given constraint and none worked
solve2 t k (x:xs) s grid
  | newg == Nothing = solve2 t k xs s grid -- the solution x did not work, try the next one
  | otherwise = newg
  where  
    newg = solve1 s (extendGrid t k x grid [])

-- fill the gaps in the grid where there are no skyscrapers yet
fillGaps :: Grid -> Maybe Grid
fillGaps grid =
  try possibleMoves [1..gridSize] $ Just grid
  where
    gridSize = size grid
    possibleMoves = [(r, c) | (r, row) <- zip [0..gridSize-1] grid, (c, col) <- zip [0..gridSize-1] row, col == 0]
    
{-
  arg 0 : locations in the grid that are empty with no skyscrapers
  arg 1 : heights that have not been tried for the first location in arg 0
  arg 2 : grid that is to be extendet
  
  a depth-first search function that tries to place the missing skyscrapers in the grid
-}
try :: [(Int, Int)] -> [Int] -> Maybe Grid -> Maybe Grid
--try m h g | trace (show m ++ " " ++ show h ++ " " ++ show g) False = undefined
try _ _ Nothing = Nothing
try [] _ g = g -- no posible moves
try _ [] _ = Nothing -- tried all heights and none worked
try ((y, x):ms) (h:hs) g
  | newg == Nothing = try ((y, x):ms) hs g
  | otherwise = newg
  where
    newg = try ms [1..(size $ just g)] $ fill y x h $ just g
   
-- is the skyscraper of height h in the row y and column x possible?
checkMove :: Int -> Int -> Int -> Grid -> Bool
checkMove y x h grid =
  (all (/=h) $ grid !! y) && (all (/=h) [get grid r x | r <- [0..(size grid)-1]]) -- in every row and column all the skyscrapers must be different

-- return if possible a new grid with a skyscraper of height h in row y and column x
fill :: Int -> Int -> Int -> Grid -> Maybe Grid
fill y x h grid
  | not $ checkMove y x h grid = Nothing
  | otherwise = Just [if y == r then [if x == c then h else col | (c, col) <- zip [0..(size grid)-1] row] 
                      else row | (r, row) <- zip [0..(size grid)-1] grid]

{-
  incorporate the row or column in to the grid
  arg 0 : is it a ROW or COLUMN
  arg 1 : index of the row or column
  arg 2 : actual values in the row or column
  arg 3 : grid that is to be extendet
  arg 4 : result grid accumulator
  
  the row or column already satisfy the constraints imposed by the user
  the only requirement is that non zero skyscrapers of the grid that intersect
  with the new row or column have the same value as the new row or column
  and that there won't be any same height skyscrapers on any row or column of the grid
-}
extendGrid :: Type -> Int -> [Int] -> Grid -> Grid -> Maybe Grid

extendGrid ROW 0 row1 (row2:grid) acc
  | newrow == Nothing = Nothing
  | any (\row -> any (\(x, y) -> x == y) $ zip row1 row) grid = Nothing -- same skyscrapers in the same column are not allowed
  | otherwise = Just $ acc ++ (just newrow):grid
  where
    newrow = extendRow row1 row2 []
extendGrid ROW k row1 (row2:grid) acc
  | any (\(x, y) -> x == y) $ zip row1 row2 = Nothing -- same skyscrapers in the same column are not allowed
  | otherwise = extendGrid ROW (k - 1) row1 grid $ acc ++ [row2]

extendGrid COLUMN _ [] [] acc = Just acc  
extendGrid COLUMN k (c:col) (row:grid) acc
  | newrow == Nothing = Nothing
  | otherwise = extendGrid COLUMN k col grid $ acc ++ [just newrow]
  where
    newrow = extendColumn k c row

{-
  arg 0 : new row
  arg 1 : old row
  arg 2 : result row accumulator
  
  extend the old row with the new row if it is possible
-}
extendRow :: [Int] -> [Int] -> [Int] -> Maybe [Int]
extendRow [] [] acc = Just acc
extendRow (x:xs) (y:ys) acc
  | y == 0 = extendRow xs ys $ acc ++ [x]
  | x == y = extendRow xs ys $ acc ++ [x]
  | otherwise = Nothing

{-
  arg 0 : index of the column in the grid
  arg 1 : value of the column for particular row
  arg 2 : old row
  
  extend the row with the element from the column if it is possible
-}
extendColumn :: Int -> Int -> [Int] -> Maybe [Int]
extendColumn 0 c (r:row)
  | any (==c) row = Nothing -- same skyscrapers in the same row are not allowed
  | r == 0 = Just (c:row) -- no skyscraper in this place yet, put one
  | c == r = Just (c:row)
  | otherwise = Nothing
  
extendColumn k c (r:row)
  | c == r = Nothing -- same skyscrapers in the same row are not allowed
  | rest == Nothing = Nothing
  | otherwise = Just (r : just rest)
  where
    rest = extendColumn (k - 1) c row

just :: Maybe a -> a
just (Just x) = x

-- different puzzles that can be solved
{-
   -3-1--
   |++++|
   |++++|3
  4|++++|
   |++++|
   ------
-}
p1 = "4  0 2 4  0 1 -3  1 0 3  1 2 1"

{-
   --2---
   |++++|3
  3|++++|2
   |++++|3
  3|++++|
   --2---
-}
p2 = "4  0 0 -3  0 1 3  0 1 -2  0 2 -3  0 3 3  1 1 2  1 1 -2"

{-
   ---24-
   |++++|
  1|++++|
   |++++|
  3|++++|1
   ---2--
-}
p3 = "4  0 1 1  0 3 3  0 3 -1  1 2 2  1 2 -2  1 3 4"

{-
   --1-3--
   |+++++|
  2|+++++|2
  3|+++++|
   |+++++|1
   |+++++|2
   -------
-}
p4 = "5  0 1 2  0 1 -2  0 2 3  0 3 -1  0 4 -2  1 1 1  1 3 3"

{-
   --31---
   |+++++|
  3|+++++|
  2|+++++|4
  2|+++++|
   |+++++|3
   ----3--
-}
p5 = "5  0 1 3  0 2 2  0 2 -4  0 3 2  0 4 -3  1 1 3  1 2 1  1 3 -3"

-- try: "test p1" to see the result of the puzzle to the p1 constraints specification from above
test :: String -> Maybe Grid
test spec =
  solve1 (quicksort (\(t, k, x) -> length x) solutions) (Just (initGrid gridSize))
  where
    (gridSize, solutions) = decodeGrid spec
   
gridToStr :: Grid -> String -> String
gridToStr [] acc = acc
gridToStr (row:grid) acc = gridToStr grid $ acc ++ concat [show r | r <- row] ++ (if grid == [] then "" else "\n")

skyscrapers =
  do
    putStr "enter constraints: "
    spec <- getLine
    let
      (gridSize, solutions) = decodeGrid spec
      -- sort the solutions before solving the puzzle, from the most to the least specific
      result = solve1 (quicksort (\(t, k, x) -> length x) solutions) (Just (initGrid gridSize))
    putStrLn $ maybe "No solution found!" (\grid -> "solution:\n" ++ gridToStr grid []) result
 
main = skyscrapers

