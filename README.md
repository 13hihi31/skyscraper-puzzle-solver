# Skyscraper Puzzle Solver

A solver of the skyscraper puzzle of any size. Tested on 4x4 and 5x5 grids.

![](skyscraper_puzzle.jpg "Skyscraper Puzzle Visualization")